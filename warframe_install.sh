#!/bin/bash

SHOW_BRIEF() {
cat << EOH

Usage:
	./warframe_install.sh download [release]
	./warframe_install.sh [--no-update|--force-update] [--overwrite] extract [proton_tar]
	./warframe_install.sh [--no-update|--force-update] [--overwrite] proton [release|proton_tar]
	./warframe_install.sh [-y] vulkan
	./warframe_install.sh after
	./warframe_install.sh [--no-update|--force-update] [--overwrite] [-y] all [release|proton_tar]
	./warframe_install.sh help

EOH
}

SHOW_HELP() {
cat << EOH
Prepare to run Warframe (and perhaps other Windows games) on Linux
using GloriousEggroll's custom version of Proton.

This script last updated 2020-07-31.

Usage:
	./warframe_install.sh download [release]
		Download GloriousEggroll's custom version of Proton from:
		https://github.com/GloriousEggroll/proton-ge-custom/releases
		If release is specified, download that specific release.
		Use "latest" to specify the latest stable release.
		Use a tag name (eg "5.2-GE-1") to specify a specific release.
		If ommited, downloads release index 0 in the list of releases,
		which should be the latest release, including unstable versions.
		Accepts a github api URL as well;
		Must return either a single release or a list of releases.
		If started from your home directory, places archive under ~/Downloads.
		Otherwise, output is placed is current working directory.

	./warframe_install.sh [--no-update|--force-update] [--overwrite] extract [proton_tar]
		Install GloriousEggroll's custom version of Proton.
		Must be downloaded first from:
		https://github.com/GloriousEggroll/proton-ge-custom/releases
		It is recommended to use the latest release,
		even if it is labelled testing.
		proton_tar may contain the path of the downloaded tar.
		Otherwise, the current directory and ~/Downloads are checked.
		Will not extract if extraction dir already exists, unless --overwrite is used.
		By default, creates and updates a special "Proton-GE-latest" entry
		for ease in updating. --no-update disables this.
		--force-update will update even if extraction dir already exists.
		Set your games to use "Proton-GE-latest", and you won't have to change
		the settings of every game each time you update proton any more.
		Of course, you can always still chose a specific version for some games.

	./warframe_install.sh [--no-update|--force-update] [--overwrite] proton [release|proton_tar]
		Do both previous actions (download and extract).
		If 2nd parameter contains :// (colon slash slash), it is interpreted
		as a github api url, which will return release information in JSON.
		If 2nd parameter contains / (single forward slash), it is interpreted
		as a local path to the tar of Proton, and nothing is downloaded.
		Otherwise, it is interpteted as the release name to download.
		Therefore, use ./file to indicate a file in the current directory.

	./warframe_install.sh [-y] vulkan
	./warframe_install.sh [-y] driver
	./warframe_install.sh [-y] drivers
		Install Vulkan driers. (Both 64 and 32-bit, if applicable.)
		If -y is specified, install proceeds without prompting.

	./warframe_install.sh after
		Print instructions on what you must do manually after
		running this script.

	./warframe_install.sh [--no-update|--force-update] [-y] [--overwrite] all [release|proton_tar]
	./warframe_install.sh [--no-update|--force-update] [-y] [--overwrite] "*" [release|proton_tar]
		Do all three previous actions.
		proton -> vulkan -> after

	./warframe_install.sh help
	./warframe_install.sh --help
		Print this help.

	There is LIMITED support for performing multiple actions at once.
	If more than one action is specified, be aware that any optional
	parameters after the action name become mandatory to all options
	before the last.
	For example,
		./warframe_install.sh vulkan extract
	or
		./warframe_install.sh extract "" vulkan
	will do what you intend (albeit in different orders), while
		./warframe_install.sh extract vulkan
	will attempt to extract a tar file named "vulkan".
	Also, make sure you specify a flag such as --force-update or -y
	BEFORE the relevant action, or it will have no effect.
EOH
}
SHOW_AFTER() {
BEGIN after
cat << 'EOA'

OK. The automatic bit is done. Here's what you need to do manually:

1) Make sure the graphics drivers for your video card are installed.
	If Warframe complains about DirectX 9 being End of Life,
	it means that the graphics drivers are not installed properly.
	You need the latest proprietary drivers for your card, available
	from the Ubuntu Driver Manager.

2) Launch Steam.
	If Steam was already open, exit completely and relaunch it.
	If you do not properly restart Steam, you WILL NOT see your
	new version of Proton! You must specifically tell Steam to exit,
	not just close all of its windows.

You have two options:
	A) Set the individual game (in this example Warframe)
		to use the version of Proton just installed.
	B) Set ALL applicable games to default to using the version
		of Proton just installed. Note that games can be
		overriden to use a different version using option A.

OPTION A: (Set games individually)

3A) Go to the Warframe page in your library.

4A) Open Warframe's properties.
	Either
		In the side pannel list of games
		Right click on Warframe
		Choose "Properties..."
	OR
		Click the gear button towards the right ("Manage")
		Choose "Properties..."

5A) In the "Warframe - Properties" popup, on the GENERAL tab,
	check the box at the bottom labelled
	"Force the use of a specific Steam Play compatability tool"

6A) Select the custom version of Proton you just installed,
	which should be near the top of the list and contain "-GE-".
	Alternatively, select "Proton-GE-latest" to always use the
	most recent version of Proton installed with this tool.

7A) Click CLOSE.

8A) Click INSTALL.

OPTION B: (set all Windows games to use Proton)

3B) Open the Steam Settings.
	Either
		In the upper left of the main window,
		select Steam and then Settings from that menu.
	OR
		Right click the system tray icon.
		Select Settigns.

4B) From the headings on the left of the Settings window,
	select "Steam Play" (near the bottom).

5B) Check "Enable Steam Play for all other titles".

6B) In the "Run other titles with:" drop down,
	select the version of Proton just installed,
	which should be near the top of the list and contain "-GE-".
	Alternatively, select "Proton-GE-latest" to always use the
	most recent version of Proton installed with this tool.
	Click "OK".

7B) Steam will inform you that it must restart for the change to
	take effect. Click "Restart Steam" to do so.
	( After Steam has restarted, if you go back to this option,
	  you may see it unchecked, as if you had never enabled it.
	  THIS IS A KNOWN BUG IN STEAM. The option has taken effect,
	  but Steam won't show it enabled on this screen. If you
	  actually try installing a game, it should work even though
	  it appears unchecked here. )

There. That wasn't so bad, was it? ^_^

NOW DON'T FORGET! Once Warframe finishes installing,
you need to tweak some settings in game for better performance:

- Warframe is problematic with vsync.
	Turn it off or on in game, do not set Auto.
- Warframe needs a set a frame limit in game.
	Unlimited framerate can cause slowdowns.
- Warframe on Nvidia you may need to disable GPU Particles
	in game otherwise the game can freeze randomly.
	On AMD they work fine.

Based on instructions from helpful users and those found on
https://github.com/GloriousEggroll/proton-ge-custom
This installer is MIT liscenced and was written by
@NerdyWhiteGuy#5108 on https://discord.gg/6y3BdzC

    _______________________________________________
   /                                               \
  |  Hey there! A lot of information has just been  |
  |  printed to your terminal. Make sure to scroll  |
  |  up and read it. In particular, read the part   |
  |  starting with "OK. The automatic bit is done." |
  |  and ending here.   Have a nice day! :-)        |
   \  _____________________________________________/
    \/
     \ O
      -+-
       |
      / \
EOA
END
}

BEGIN() {
	CURRENT_ACTION="$1"
	echo -e " - \e[1mBeginning action: $1\e[0m"
}
END() {
	# Use the value of $1 if it is not the empty string.
	# Otherwise, use $CURRENT_ACTION.
	echo
	echo -e " - \e[1mFinished action: ${1:-$CURRENT_ACTION}\e[0m"
}

DETECT_DISTRO() {
	# Don't re-test if distro has already been found.
	[ -n "$DISTRO" ] && return
	if type apt-get >& /dev/null >&1 ; then
		DISTRO=debian
	else
		echo "Unable to determine distro." 1>&2
		return 1
	fi
	echo "Distro detected as $DISTRO."
	echo
}

DOWNLOAD_PROTON() {
	# JSON parsing ideas: https://stackoverflow.com/a/1955555
	BEGIN "download"
	exec 3>&1
	export GIT_RELEASE_ID="$1"
	# Use the contents of $1 or the string "latest" to pick which version to fetch.
	read SIZE DOWNLOAD < <( python3 -c 'import os, sys, requests
fd3 = open(3,"w")
URL_PREFIX = "https://api.github.com/repos/GloriousEggroll/proton-ge-custom/releases"
release_id = os.environ["GIT_RELEASE_ID"]
if release_id == "":
	url = URL_PREFIX
else:
	if release_id == "latest":
		url = URL_PREFIX + "/latest"
	else:
		try: # If release_id was a full URL, just use that.
			release_id.index("://")
			url = release_id
		except ValueError: # Otherwise, treat it as a tag name.
			url = URL_PREFIX + "/tags/" + release_id
result = requests.get(url, headers={"Accept":"application/json"})
release = result.json()
if result.status_code >= 300:
	print(release, file=fd3)
	exit(1)
if isinstance(release, list): # If we got a list, operate on first item.
	release = release[0]
assets = release["assets"]
largest = len(assets)-1
size = assets[largest]["size"]
for i in range(1,len(assets)):
	if assets[i]["size"] > size:
		largest = i
		size = assets[i]["size"]
print("In release", release["tag_name"]+",", "file index", largest, "has a size of", size, "and is downloadable via", file=sys.stderr)
print(assets[largest]["browser_download_url"], file=sys.stderr)
print(file=fd3)
print("# BEGIN RELEASE NOTES #", file=fd3)
print(file=fd3)
print(release["body"], file=fd3)
print(file=fd3)
print("# END RELEASE NOTES #", file=fd3)
print(size, assets[largest]["browser_download_url"])' )
	if [ $? = 0 ] && [ -n "$DOWNLOAD" ]; then
		if [ "$HOME" = "$PWD" ] && [ -d ~/Downloads ]; then
			INPUT=~/"Downloads/${DOWNLOAD##*/}"
		else
			INPUT="./${DOWNLOAD##*/}"
		fi
		echo
		if [ -s "$INPUT" ]; then
			echo "File $INPUT already exists."
			REAL_SIZE=`wc -c "$INPUT" | cut -d ' ' -f 1`
			if [ "$REAL_SIZE" = "$SIZE" ]; then
				echo "File has correct size of $REAL_SIZE bytes."
				echo "Delete or rename this file to download again."
				END
				return 0
			else
				echo "Existing file has INCORRECT size of $REAL_SIZE bytes."
				echo "Existing file SHOULD have a size of $SIZE bytes."
				echo "REMOVING existing file and RETRYING download."
				rm "$INPUT"
				if [ -z "$OVERWRITE" ]; then
					echo "In case of in-place update or a corrupted extraction,"
					echo "we will OVERWRITE any previous output."
					OVERWRITE=auto
				fi
			fi
		fi
		if curl -L -o "$INPUT" "$DOWNLOAD"; then
			echo "Saved file as $INPUT"
			REAL_SIZE=`wc -c "$INPUT" | cut -d ' ' -f 1`
			if [ "$REAL_SIZE" = "$SIZE" ]; then
				END
				return 0
			else
				echo "Downloaded file has wrong size!" 1>&2
				echo "File should have size $SIZE," 1>&2
				echo "but actually has size $REAL_SIZE." 1>&2
				echo "NOT continuing. Run again to retry download." 1>&2
				END
				return 1
			fi
		else
			echo "Download failed. Error code: $?" 1>&2
			END
			return 1
		fi
	else
		echo "Failed to find URL of release tarball." 1>&2
		END
		return 2
	fi
}

FIND_OUTPUT() {
	OUTPUT="$1"
	if ! [ -d "$1" ]; then
		echo "$OUTPUT: No such file or directory" 1>&2
		return 1
	fi
}

FIND_INPUT() {
	INPUT="`find "$1" -maxdepth 1 -iname 'Proton*-GE-*.tar*' -print0 | sort -z | tail -z --lines 1 | head --bytes -1`"
	[ -n "$INPUT" ]
}

SETUP_LATEST() {
	# Create a "latest" entry.
	LATEST_VDF="$OUTPUT/Proton-GE-latest/compatibilitytool.vdf"
	if ! [ -d "${LATEST_VDF%/*}" ] && [ -a "${LATEST_VDF%/*}" ] ; then
		echo "${LATEST_VDF%/*} exists, but is not a directory." 1>&2
		echo "NOT creating Proton-GE-latest entry." 1>&2
	else
		mkdir -p "${LATEST_VDF%/*}"
		echo "Setting Proton-GE-latest to point to $EXTRACTED"
		cat > "$LATEST_VDF" << EOVDF
"compatibilitytools"
{
  "compat_tools"
  {
    "Proton-GE-latest" // Internal name of this tool
    {
      // Can register this tool with Steam in two ways:
      //
      // - The tool can be placed as a subdirectory in compatibilitytools.d, in which case this
      //   should be '.'
      //
      // - This manifest can be placed directly in compatibilitytools.d, in which case this should
      //   be the relative or absolute path to the tool's dist directory.
      "install_path" "../$EXTRACTED"

      // For this template, we're going to substitute the display_name key in here, e.g.:
      "display_name" "Proton-GE-latest (Currently $EXTRACTED)"

      "from_oslist"  "windows"
      "to_oslist"    "linux"
    }
  }
}
EOVDF
	fi
}

CAREFUL_DELETE() {
	if [ -z "$1" ]; then
		echo "FATAL ERROR: ATTEMPTED TO DELETE THE EMPTY STRING!" 2>&1
		exit 3
	fi
	if pushd "$1" > /dev/null; then
		RMDIR="$PWD"
		popd > /dev/null
		if [ "$RMDIR" = "$PWD" ]; then
			echo "FATAL ERROR: ATTEMPTED TO DELETE THE CURRENT DIRECTORY!" 2>&1
			exit 3
		fi
		if [ "$RMDIR" = "$HOME" ] || [[ "$RMDIR" =~ ^/home/[^/]+$ ]]; then
			echo "FATAL ERROR: ATTEMPTED TO DELETE A HOME DIRECTORY!" 2>&1
			exit 3
		fi
		if ! [[ "$RMDIR" =~ ^/home/[^/]+/ ]]; then
			echo "FATAL ERROR: ATTEMPTED TO DELETE OUTSIDE OF YOUR HOME DIRECTORY!" 2>&1
			exit 3
		fi
		if [ -L "$1" ]; then
			echo "Removing symlink…"
			rm -v "$1"
		else
			echo "Removing directory: $RMDIR"
			rm --one-file-system -rI "$RMDIR"
		fi
	else
		echo "Could not enter $1" 2>&1
		echo "NOT attempting to delete." 2>&1
		return 1
	fi
}

EXTRACT_PROTON() {
	# Based on instructions from https://github.com/GloriousEggroll/proton-ge-custom
	BEGIN "extract"
	echo "Extract the downloaded tar of Proton into Steam's compatability tools folder."
	echo
	if ! FIND_OUTPUT ~/.steam/root; then
		if ! FIND_OUTPUT ~/.var/app/com.valvesoftware.Steam/data/Steam; then
			echo "Unable to find Steam install in the usual places."
			echo "Make sure you've installed it and run it at least once."
			exit 1
		fi
	fi 1>&2
	if [ -z "$INPUT" ]; then
		if [ -z "$1" ]; then
			if ! FIND_INPUT .; then
				if ! FIND_INPUT ~/Downloads; then
					echo "Proton*-GE-*.tar*: No such file or directory"
					echo "Unable to find Proton installer."
					echo "Please download the latest release (testing are OK) from"
					echo "https://github.com/GloriousEggroll/proton-ge-custom/releases"
					echo "and place it in either the current directory or ~/Downloads"
					exit 1
				fi
			fi 1>&2
		else
			INPUT="$1"
		fi
	else
		echo "Using preestablished input file: $INPUT" 1>&2
	fi
	OUTPUT="$OUTPUT/compatibilitytools.d"
	mkdir -p "$OUTPUT"
	echo "Preparing to extract Proton into the following directory:"
	echo "$OUTPUT"
	# Get dir name from inside archive.
	EXTRACTED="`tar -tf "$INPUT" | head -n 1`"
	# Trim trailing /
	EXTRACTED="${EXTRACTED%%/*}"
	if [ -z "$EXTRACTED" ]; then
		echo "Reading from tar '$INPUT' FAILED!" 1>&2
		END
		return $code
	fi
	if [ -a "$OUTPUT/$EXTRACTED" ]; then
		if [ "$OVERWRITE" = "auto" ] || [ "$OVERWRITE" = "yes" ]; then
			echo "$OUTPUT/$EXTRACTED already exists. DELETING." 1>&2
			if ! CAREFUL_DELETE "$OUTPUT/$EXTRACTED" || [ -a "$OUTPUT/$EXTRACTED" ]; then
				echo "NOT deleted. Aborting extract."
				if [ "$UPDATE_LATEST" = "force" ]; then
					echo
					echo "Setting up of \"Proton-GE-latest\" entry forced. (via --update-force)"
					SETUP_LATEST
				fi
				END
				return 1
			fi
		else
			echo "$OUTPUT/$EXTRACTED already exists. NOT extracting over it." 1>&2
			echo "Delete or rename the existing copy to extract to this directory." 1>&2
			if [ "$UPDATE_LATEST" = "force" ]; then
				echo
				echo "Setting up of \"Proton-GE-latest\" entry forced. (via --update-force)"
				SETUP_LATEST
			fi
			END
			return 0
		fi
	fi
	if # Test for success of entire extraction process.
		if type pv >& /dev/null >&1 ; then # If pv is available, use it.
			pv "$INPUT" | tar -x -C "$OUTPUT" -z
		else
			echo "(Consider installing the pv utility to see a progress bar here.)"
			tar -x -C "$OUTPUT" -zf "$INPUT"
		fi
	then
		echo
		if [ "$UPDATE_LATEST" != "no" ]; then
			SETUP_LATEST
		else
			echo "NOT setting  \"Proton-GE-latest\" entry. (Dissabled via --no-update)"
		fi
		END
		return 0
	else
		code=$?
		echo "Extracting tar FAILED! Code $code" 1>&2
		END
		return $code
	fi
}

INSTALL_PROTON() {
	BEGIN "proton"
	if [ -z "$1" ] && [[ "$1" != *://* ]] && [[ "$1" == */* ]]; then
		# Is not the empty string,
		# does NOT contain colon slash slash,
		# DOES contain a slash.
		# Therefore, it is a path.
		# Note that [[ ]] uses pattern matching while [ ] does not.
		EXTRACT_PROTON "$1"
	else
		# Otherwise, it is not a path.
		DOWNLOAD_PROTON "$1" && EXTRACT_PROTON
	fi
	code=$?
	END "proton"
	return $code
}

INSTALL_VULKAN() {
	BEGIN "vulkan"
	echo "Installing Vulkan graphics drivers."
	echo "Required to actually launch the game."
	echo
	DETECT_DISTRO
	case "$DISTRO" in
		debian)
			# Architechture detection: https://akrabat.com/installing-32-bit-packages-on-ubuntu-14-04/
			ARC="`dpkg --print-architecture`"
			# Detect and install Mesa Vulkan drivers for native architecture.
			if dpkg-query -W --showformat='${Status}\n' mesa-vulkan-drivers:$ARC 2> /dev/null | grep -qF "install ok installed"; then
				echo "mesa-vulkan-drivers:$ARC appears to be already installed."
			else
				echo "Installing mesa-vulkan-drivers:$ARC..."
				sudo apt-get $AUTO_INSTALL install mesa-vulkan-drivers:$ARC
			fi
			# If architecture is amd64 (aka 64-bit), also check i386 (aka 32-bit).
			if [ "$ARC" = "amd64" ]; then
				echo
				if dpkg-query -W --showformat='${Status}\n' mesa-vulkan-drivers:i386 2> /dev/null | grep -qF "install ok installed"; then
					echo "mesa-vulkan-drivers:i386 appears to be already installed."
				else
					if ! dpkg --print-foreign-architectures | grep -q "i386" ; then
						echo "Adding the i386 architechture..."
						sudo dpkg --add-architecture i386
						sudo apt-get update
						echo
					fi
					echo "Installing mesa-vulkan-drivers:i386..."
					sudo apt-get $AUTO_INSTALL install mesa-vulkan-drivers:i386
				fi
			fi
			;;
		*)
			echo "Can't install Vulkan drivers automatically." 1>&2
			echo "It is up to you to ensure these drivers are installed." 1>&2
			;;
	esac
	END
}

PROCESS_ARG() {
	CONSUME_ARGS=1
	case $1 in
		"")
			SHOW_BRIEF
			exit 2
			;;
		help|--help)
			SHOW_HELP
			;;
		download)
			DOWNLOAD_PROTON "$2"
			CONSUME_ARGS=2
			;;
		extract)
			EXTRACT_PROTON "$2"
			CONSUME_ARGS=2
			;;
		proton)
			INSTALL_PROTON "$2"
			CONSUME_ARGS=2
			;;
		vulkan)
			INSTALL_VULKAN
			;;
		after)
			SHOW_AFTER
			;;
		"*"|all)
			INSTALL_PROTON "$2" && INSTALL_VULKAN && SHOW_AFTER
			# Repeated && above is to abort if an error is encountered.
			CONSUME_ARGS=2
			;;
		--no-update)
			UPDATE_LATEST=no
			;;
		--force-update)
			UPDATE_LATEST=force
			;;
		--overwrite)
			OVERWRITE=yes
			;;
		-y)
			# The value of this variable is conveniently the flag
			# passed to many tools to disable prompting when
			# installing new software. Of course, not all tools for
			# all distros will use the same flag.
			AUTO_INSTALL=-y
			;;
		*)
			echo "Unknown action: $1" 1>&2
			SHOW_BRIEF
			exit 2
			;;
	esac
}

PROCESS_ARG "$1" "$2"
# shift fails (and shifts nothing) if it tries to shift more args than exist.
# Therefore, attempt to shift no more than the maximum number of args.
shift "$(( CONSUME_ARGS > $# ? $# : CONSUME_ARGS ))"
while (( $# >= 1 )); do
	PROCESS_ARG "$1" "$2"
	shift "$(( CONSUME_ARGS > $# ? $# : CONSUME_ARGS ))"
done
