# Basic Usage

0. Delete all old versions of this script you may have downloaded previously. Otherwise, your web browser may automatically change the name of the file when you download it.
1. [**Download the script**](https://gitlab.com/nerdywhiteguy/warframe-install/-/raw/master/warframe_install.sh?inline=false) and save it in your Downloads directory (`~/Downloads`). Make sure the file is saved as `warframe_install.sh`
2. Open a Terminal (Ctrl+Shift+T on many systems).
3. `cd ~/Downloads`
4. `chmod +x ./warframe_install.sh`
5. `./warframe_install.sh all`
6. Follow on screen instructions.

# What Does This Script Do?

1. Installs basic dependencies. (mesa and vulkan drivers, both x64 and x86)
2. Installs GE's Proton inside of Steam. (Prints release notes as it does so.)
3. Tells you what options to change inside Steam.
4. Tells you what options to change inside Warframe.
5. Sets it up so that, in the future, you won't need to change Steam options when you update Proton with this script. (Set Steam to use "Proton-GE-latest" as the Proton version, which this script updates whenever it downloads a new version. If you don't want to use this special "version", just pick the Proton version you want like you normally would.)

# What Does This Script NOT Do?

- Change Steam options for you.
- Change game options for you.

If anyone can find a way to automatically change options in Steam or Warframe, please let me know.

# Why Use This Script?

Do you want to update Proton with a oneliner? Are you tired of changing Steam options every time you update Proton? If you answered "yes" to either of these questions, this script may be for you! Act now, and for only one additional payment of $19.95, we'll throw in some extra sarcasm. Wow! What a deal!

Look, IDK, man. I think this script makes things easier that I'd otherwise have to do manually. If you'd prefer to do them manually, be my guest. I, for one, enjoy oneliners, and hope that you'll like this oneliner, too.

# Additional Options

To simply update Proton and not print comprehensive instructions after every run, use `./warframe_install.sh proton` instead of #5.

`./warframe_install.sh help` will show additional options and more details on exactly what each option does.

[The source](https://gitlab.com/nerdywhiteguy/warframe-install/-/raw/master/warframe_install.sh) was designed to be easy to read as well, with help messages at the very beginning of the code. See these for more comprehensive help.

## A note for complete command line newbies

When reading a syntax guide for something on the command line, `a [b] c` means that you *may* type `b` or not. That is, either `a b c` or `a c` would be valid.

A vertical bar or pipe means "or". `d [e|f] g` means you can put either `e` or `f` there, such as `d e g` or `d f g` and both would be valid. Note that it's inside `[]`, so it's still optional. (Meaning `d g` is also valid.)

This syntax is a bit informal, so you're exptected to understand when it makes sense to type something literally and when to replace it with something sensible. Things starting with a dash are a flag or parameter of some sort, so are generally typed literally.

So what that means is: When I say `./warframe_install.sh [--no-update|--force-update] proton [release|proton_tar]`, what I mean is "After typing the name of the script, you may specify the option `--no-update` or the option `--force-update` but you don't have to. Then type `proton`. Then you may specify a release or a Proton tar file, but you don't have to."

Going off of the above example, some valid command lines include (but are not limited to):
- `./warframe_install.sh --no-update proton` (Update Proton but don't update the Proton-GE-latest version.)
- `./warframe_install.sh --force-update proton` (Update Proton and update the Proton-GE-latest version to point to this version even if Proton was already updated.)
- `./warframe_install.sh proton 5.8-GE-2-MF` (Install Proton version 5.8-GE-2-MF. This is one of GloriousEggroll's release tags. If you don't specify one, or specify the empty string, it will get the most recent tag. Use the tag "latest" to use the latest *stable* release. [Here are all of the tags GloriousEggroll has used](https://github.com/GloriousEggroll/proton-ge-custom/tags).)
- `./warframe_install.sh --force-update proton 5.8-GE-2-MF` (Install Proton version 5.8-GE-2-MF. Update the Proton-GE-latest version to 5.8-GE-2-MF even if 5.8-GE-2-MF was already installed.)
- `./warframe_install.sh proton ~/Downloads/Proton-5.8-GE-testing.tar.gz` (Install a version of Proton you've already downloaded, perhaps from another source. This is the path to a file you've already downloaded. Maybe GE has blessed you with a link to a test build? Download it and then put the path to the file here after it's downloaded.)
- **The above are some random examples of possible valid command lines for advanced usage. If you just want to get up and running quickly, see the beginning of this document for [Basic Usage](#basic-usage).**

For refference, using square brackets like this comes from the [extended Backus–Naur form](https://en.wikipedia.org/wiki/Extended_Backus–Naur_form).
